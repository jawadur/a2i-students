import dash_core_components as dcc
import dash_html_components as html
import dash_auth
from dash.dependencies import Input, Output
import dash_table_experiments as dt

from app import app
from apps import local, foreign
from os import sys, path


server = app.server

sys.path.append(path.dirname(path.abspath(__file__)))

VALID_USERNAME_PASSWORD_PAIRS = [
    ['a2i', 'a2i']
]

auth = dash_auth.BasicAuth(
    app,
    VALID_USERNAME_PASSWORD_PAIRS
)

app.css.append_css({'external_url': 'https://codepen.io/kmjawadurrahman/pen/KxVwzo.css'})

app.layout = html.Div([
        dcc.Location(id='url', refresh=False),
        html.Div(id='page-content'),
        html.Div(dt.DataTable(rows=[{}]), style={'display': 'none'})
    ])


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/':
        return local.layout
    elif pathname == '/foreign':
        return foreign.layout
    else:
        return '404'

if __name__ == '__main__':
    app.run_server(debug=True)
