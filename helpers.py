import dash_html_components as html
import dash_core_components as dcc
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import json
import requests
import datetime
from ast import literal_eval
import plotly.graph_objs as go
import plotly.figure_factory as ff

chart_layout_template = dict(plot_bgcolor="#F3F3F1",
                    paper_bgcolor="#CAD2D4",
                    font=dict(color='black'),
                    margin=dict(
                        l=70,
                        r=50,
                        b=50,
                        t=85
                    ),
                    legend=dict(
                        bgcolor='#F3F3F1',
                        bordercolor='#F3F3F1'
                    ),
                    xaxis=dict(autorange=True))

continent_scope_mapper = {'North America': 'north america', 'South America': 'south america', 'Asia': 'asia',
                            'Africa': 'africa', 'Europe': 'europe', 'Other': 'world'}

class FixedLayoutHelpers:
    def __init__(self, dashboard_name):
        self.dashboard_name = dashboard_name

    def generate_firstrow_layout(self, button_class_list,
                                    button_name_list=['University Students', 'Foreign Students'],
                                    href_list = [ '/', '/foreign']):
        firstrow_layout = list()
        for button_name, href, button_class in zip(button_name_list, href_list, button_class_list):
            firstrow_layout.append(self.generate_nav_button(button_name, href, button_class))
        return firstrow_layout

    def generate_nav_button(self, button_name, href, button_class):
        nav_button = html.A(html.Button(button_name, className='two columns ' + button_class,
            style={'margin-right':20, 'padding':0, 'white-space': 'normal', 'line-height': 15}),
            href=href)
        return nav_button

    def generate_secondrow_layout(self):
        secondrow_layout = [
            html.H3(
                self.dashboard_name,
                style={
                    'font-family': 'Helvetica',
                    "margin-top": "15",
                    "margin-bottom": "15"
                },
                className='twelve columns',
            )
        ]
        return secondrow_layout

class FileHelpers:
    def __init__(self, relative_path_filename, sheet_name='Sheet1'):
        self.relative_path_filename = relative_path_filename
        self.sheet_name = sheet_name

    def get_raw_dataframe(self):
        raw_dataframe = pd.read_excel(self.relative_path_filename, sheet_name=self.sheet_name)
        return raw_dataframe

class DataFrameHelpers:
    def __init__(self, dataframe, date_colname=None):
        self.dataframe = dataframe
        self.date_colname = date_colname
        self.month_categories = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"]
        self.month_map = {1: 'January', 2: 'February', 3: 'March', 4: 'April', 5: 'May', 6: 'June',
                    7: 'July', 8: 'August', 9: 'September', 10: 'October', 11: 'November', 12: 'December'}

    def get_available_levels(self, colname):
        available_levels = self.dataframe[colname].unique()
        return available_levels

    def ordered_categorical_options(self, available_items, categories, is_ascending=True):
        categorical_options = pd.Categorical(available_items, categories=categories, ordered=True)
        categorical_options.sort_values(inplace=True, ascending=is_ascending)
        return categorical_options

    def ordered_categorical_dataframe_column(self, dataframe, colname, categories, by=None, is_ascending=True):
        dataframe[colname] = pd.Categorical(dataframe[colname], categories=categories, ordered=True)
        dataframe.sort_values(by, inplace=True, ascending=is_ascending)
        return dataframe

    def add_datetime_attributes(self, attribute_list):
        if 'Year' in attribute_list and 'Year' not in self.dataframe.columns:
            self.dataframe['Year'] = self.dataframe[self.date_colname].apply(lambda x: x.year)
        if 'Month' in attribute_list and 'Month' not in self.dataframe.columns:
            self.dataframe['Month'] = self.dataframe[self.date_colname].apply(lambda x: x.month)
        if 'Day' in attribute_list and 'Day' not in self.dataframe.columns:
            self.dataframe['Day'] = self.dataframe[self.date_colname].apply(lambda x: x.day)
        if 'Weekday' in attribute_list and 'Weekday' not in self.dataframe.columns:
            self.dataframe['Weekday'] = self.dataframe[self.date_colname].apply(lambda x: x.weekday())
        if 'Yearday' in attribute_list and 'Yearday' not in self.dataframe.columns:
            self.dataframe['Yearday'] = self.dataframe[self.date_colname].apply(lambda x: x.tm_yday)
        if 'Week' in attribute_list and 'Week' not in self.dataframe.columns:
            self.dataframe['Week'] = self.dataframe[self.date_colname].apply(lambda x: x.isocalendar()[1])
        if 'Hour' in attribute_list and 'Hour' not in self.dataframe.columns:
            self.dataframe['Hour'] = self.dataframe[self.date_colname].apply(lambda x: x.hour)
        if 'Minute' in attribute_list and 'Minute' not in self.dataframe.columns:
            self.dataframe['Minute'] = self.dataframe[self.date_colname].apply(lambda x: x.minute)
        if 'Second' in attribute_list and 'Second' not in self.dataframe.columns:
            self.dataframe['Second'] = self.dataframe[self.date_colname].apply(lambda x: x.second)

    def map_month_names(self):
        self.dataframe['Month'] = self.dataframe.Month.map(self.month_map)

    def get_selector_options_class(self, available_items_list, items_categories, is_ascending=True):
        selector_items = self.ordered_categorical_options(available_items_list, items_categories, is_ascending=is_ascending)
        selector_options_class = [{'label': str(item), 'value': item}
                            for item in selector_items]
        return selector_options_class

    def filter_dataframe(self, year, optional_filter_selections=[], optional_filter_cols=[]):
        dff = self.dataframe[self.dataframe['Year'] == year]

        if optional_filter_cols:
            for idx, filter_col in enumerate(optional_filter_cols):
                if filter_col == 'Month' and 'Date' in dff.columns:
                    dff['Week'] = dff['Date'].apply(lambda x: x.isocalendar()[1])
                    dff_month = dff[dff[filter_col] == optional_filter_selections[idx]]
                    weeks_in_month = dff_month['Week'].unique()
                    dff = dff[dff['Week'].isin(weeks_in_month)]
                else:
                    dff = dff[dff[filter_col] == optional_filter_selections[idx]]

        return dff

class ChartHelpers:
    def __init__(self, dataframe=pd.DataFrame(), columns_list=list(), color_limits_list=list(), filter_info=dict()):
        self.performance_colors = ['rgb(215,25,28)',
                                     'rgb(253,174,97)',
                                     'rgb(255,255,191)',
                                     'rgb(166,217,106)',
                                     'rgb(26,150,65)']
        self.dataframe = dataframe
        self.columns_list = columns_list
        self.color_limits_list = color_limits_list
        self.filter_info = filter_info

class ChoroplethMapHelpers(ChartHelpers):
    mapbox_token = 'pk.eyJ1IjoiamF3YWR1ciIsImEiOiJjampneHNpcXkzY3FyM3ZtbXJ3b2cxbm9xIn0.ibU3pycC_lqVtRtLCCX_qg'

    def __init__(self, map_data_url):
        super().__init__()
        self.map_data_url = map_data_url
        self.lons = list()
        self.lats = list()
        self.text = ''
        self.sources = list()
        self.colors_dict = dict()
        self.raw_map_data = dict()
        self.name = list()

    def load_map_data(self):
        self.raw_map_data = requests.get(self.map_data_url)
        self.raw_map_data = json.loads(self.raw_map_data.text)

    def filter_and_init_map_data(self, available_property_items, name_mapper_dict, property):
        map_data_list = [row for row in self.raw_map_data['features'] if row['properties'][property] in available_property_items]
        for idx, row in enumerate(map_data_list):
            map_data_list[idx]['properties'][property] = self.map_property_items(row, name_mapper_dict, property)
        map_data_json_dump = json.dumps({"type":"FeatureCollection", "features": map_data_list})
        filtered_map_data = json.loads(map_data_json_dump)

        for k in range(len(filtered_map_data['features'])):
            try:
                country_coords=np.array(filtered_map_data['features'][k]['geometry']['coordinates'][0])
                if len(country_coords.shape) == 3:
                    m, M =country_coords[:,:,0].min(), country_coords[:,:,0].max()
                    self.lons.append(0.5*(m+M))
                    m, M =country_coords[:,:,1].min(), country_coords[:,:,1].max()
                    self.lats.append(0.5*(m+M))
                else:
                    m, M =country_coords[:,0].min(), country_coords[:,0].max()
                    self.lons.append(0.5*(m+M))
                    m, M =country_coords[:,1].min(), country_coords[:,1].max()
                    self.lats.append(0.5*(m+M))
            except Exception as e:
                self.lons.append(np.nan)
                self.lats.append(np.nan)

        for feat in filtered_map_data['features']:
            self.sources.append({"type": "FeatureCollection", 'features': [feat]})
        return filtered_map_data

    def map_property_items(self, map_data_row, name_mapper_dict, property):
        mapped_property_item = name_mapper_dict[map_data_row['properties'][property]]
        return mapped_property_item

    def get_property_items_list(self, available_property_items, name_mapper_dict, property):
        property_items_list = [self.map_property_items(row, name_mapper_dict, property) \
                            for row in self.raw_map_data['features'] if row['properties'][property] in available_property_items]
        return property_items_list

    def generate_hover_text_field(self, filtered_map_data, properties_list):
        text_item_list = list()
        for property in properties_list:
            text_item_list.append([self.raw_map_data['features'][k]['properties'][property] for k in range(len(self.raw_map_data['features']))\
                        if self.raw_map_data['features'][k]['properties'][property] in filtered_map_data['features'][k]['properties'][property]])

        return text_item_list

    def generate_map_figure(self, title, lat, lon, zoom, layers=True):
        data = dict(type='scattermapbox',
            lat = self.lats,
            lon = self.lons,
            mode = 'markers',
            text = self.text,
            customdata = self.name,
            marker = dict(size=15, color='brown', opacity = 0.5, symbol='circle'),
            showlegend = False,
            hoverinfo = 'text'
        )
        if not layers:
            layers = dict()
        else:
            layers=[dict(sourcetype = 'geojson',
                source = self.raw_map_data["features"][k],
                below = "water",
                type = 'fill',
                color = self.colors_dict[self.raw_map_data['features'][k]['properties']['Name']],
                opacity = 1
            ) for k in range(len(self.colors_dict))]
        layout = dict(title=title,
            plot_bgcolor="#F3F3F1",
            paper_bgcolor="#CAD2D4",
            font=dict(color='black'),
            margin=dict(
                l=70,
                r=70,
                b=50,
                t=85
            ),
            autosize=False,
            hovermode='closest',
            text=self.text,
            mapbox=dict(accesstoken=ChoroplethMapHelpers.mapbox_token,
                style='light',
                layers=layers,
                bearing=0,
                center=dict(
                    lat = lat,
                    lon = lon),
                pitch=0,
                zoom=zoom
            )
        )
        fig = dict(data=[data], layout=layout)
        return fig

def get_interaction_element(interaction_data, key='customdata'):
    if interaction_data is not None:
        try:
            interaction_element = interaction_data['points'][0][key]
        except Exception as e:
            print(e)
            interaction_element = None
    else:
        interaction_element = None
    return interaction_element

def generate_linechart(selected_univ, dataframe, y_axis_column_name, chart_title, processed=False):
    if not processed:
        filtered_df = dataframe[(dataframe['Name of University']==selected_univ)
                                                        & (dataframe[y_axis_column_name] > 0)]
        if len(filtered_df) == 0:
            fig = go.Figure(layout = chart_layout_template)
            fig['layout']['title'] = chart_title
            return fig
    else:
        filtered_df = dataframe
    x_axis_data = filtered_df['Year']
    y_axis_data = filtered_df[y_axis_column_name]
    average = round(filtered_df[y_axis_column_name].mean(), 2)
    average_data = [average for _ in range(len(x_axis_data))]
    trace0 = go.Scatter(
        x = x_axis_data,
        y = y_axis_data,
        mode = 'lines+markers',
        name = 'Annual',
        line = dict(color = 'brown',
                    width = 2),
        marker=dict(color= 'brown', size=10)
    )
    trace1 = go.Scatter(
        x = x_axis_data,
        y = average_data,
        mode = 'lines',
        name = 'Average',
        line = dict(color = 'green',
                    width = 3,
                    dash = 'dash')
    )
    try:
        average_data_annotation = average_data[0]
    except:
        average_data_annotation = 'n/a'
    annotations = [dict(xref='paper', x=0.65, y=average_data_annotation,
                        xanchor='left', yanchor='bottom',
                        text='Average: {}'.format(average_data_annotation),
                        font=dict(family='Arial',
                                  size=16),
                        showarrow=False)]

    data = [trace0, trace1]
    layout = dict(title = chart_title,
                    xaxis = dict(nticks = len(x_axis_data)),
                    showlegend=False, annotations = annotations,
                    plot_bgcolor="#F3F3F1", paper_bgcolor="#CAD2D4",
                    font=dict(color='black'),
                    margin=dict(
                        l=70,
                        r=70,
                        b=50,
                        t=85
                    )
            )
    fig = dict(data=data, layout=layout)
    return fig

def generate_barchart(x_axis_categories, y_data_list, colors_list, names_list, chart_title, barmode='stack'):
    data = list()

    for i in range(len(y_data_list)):
        data.append(go.Bar(
            x=x_axis_categories,
            y=y_data_list[i],
            marker=dict(color=colors_list[i]),
            name=names_list[i])
        )

    layout = dict(barmode=barmode,
                    title = chart_title,
                    plot_bgcolor="#F3F3F1",
                    paper_bgcolor="#CAD2D4",
                    font=dict(color='black'),
                    margin=dict(
                        l=70,
                        r=50,
                        b=50,
                        t=85
                    ),
                    legend=dict(
                        bgcolor='#F3F3F1',
                        bordercolor='#F3F3F1'
                    ),
                    xaxis=dict(autorange=True)
            )
    fig = dict(data=data, layout=layout)
    return fig

def generate_scorecard(value, title):
    return html.Div([
        html.Div(value,
                    style={'font-size': 40, 'height': '105px',
                            'line-height': '105px', 'padding': '20px',
                            'text-align': 'center', 'background-color': '#F3F3F1'}),
        html.Div(title, style={'font-size': '16', 'text-align': 'center', 'padding': '10px'})
    ], style={'background-color': "#CAD2D4", 'padding-left': '30px', 'padding-right': '30px', 'padding-top': '30px', 'margin-bottom': '8px'})
