import dash_core_components as dcc
import dash_html_components as html
import dash_table_experiments as dt
from dash.dependencies import Input, Output
import plotly.figure_factory as ff
import numpy as np
import pandas as pd
from helpers import *
import datetime
from app import app
from . import local
import json
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

dashboard_name = 'Foreign Students Dashboard'

countries_file_helpers = FileHelpers('./data/cleaned/countries.xlsx', 'Sheet1')
countries_dataframe = countries_file_helpers.get_raw_dataframe()

countrywise_foreign_file_helpers = FileHelpers('./data/cleaned/Countrywise foreign students in different universities_2017.xlsx', 'Page1')
countrywise_foreign_dataframe = countrywise_foreign_file_helpers.get_raw_dataframe()

countries_list = countrywise_foreign_dataframe['Name of University'].unique()
locations_foreign_df = local.locations_dataframe[local.locations_dataframe['Name of University'].isin(countries_list)]
countrywise_foreign_dataframe = countrywise_foreign_dataframe.merge(countries_dataframe, on='Country')
countrywise_foreign_dataframe = countrywise_foreign_dataframe.rename(columns={'Sub Total': 'Total Students'})
countrywise_foreign_dataframe_helpers = DataFrameHelpers(countrywise_foreign_dataframe, None)

available_program_list = countrywise_foreign_dataframe_helpers.get_available_levels('Program Level')
available_program_class = countrywise_foreign_dataframe_helpers.get_selector_options_class(available_program_list,
                                                                                ['Bachelor', 'Masters', 'PhD', 'Others'], is_ascending=True)

yearwise_file_helpers = FileHelpers('./data/cleaned/Yearwise foreign student statistics in different universities(edited).xlsx', 'yearwise_foreign_student_stats')
yearwise_foreign_dataframe = yearwise_file_helpers.get_raw_dataframe()

details_count_file_helpers = FileHelpers('./data/cleaned/foreign students counts.xlsx', 'Sheet1')
details_count_foreign_dataframe = details_count_file_helpers.get_raw_dataframe()
details_count_foreign_dataframe.drop_duplicates(inplace=True)
details_count_foreign_dataframe = details_count_foreign_dataframe.sort_values(by='Total Students', ascending=False)

layout_helpers = FixedLayoutHelpers(dashboard_name)
firstrow_layout = layout_helpers.generate_firstrow_layout(button_class_list=['button', 'button button-selected'])
secondrow_layout = layout_helpers.generate_secondrow_layout()

map_data_url = 'https://gist.githubusercontent.com/kmjawadurrahman/edb369357eccae6b2d52a081bb1e66c9/raw/b6b2d435b30e226b9a28496b844db0dcc616b1ff/bd-division.json'

choropleth_university_map_helpers_foreign = ChoroplethMapHelpers(map_data_url)

choropleth_university_map_helpers_foreign.load_map_data()

layout = html.Div([
    html.Div(
        firstrow_layout, className='row', style={'margin-top':20, 'margin-bottom':20}
    ),
    html.Div(
        secondrow_layout, className='row', style={'margin-bottom':20}
    ),
    html.Div(
        id='search-output-foreign', style={'display': 'none'}
    ),
    html.Div(
        id='selected-foreign-dataframe', style={'display': 'none'}
    ),
    html.Div(
        id='selected-univ', style={'display': 'none'}
    ),
    html.Div([
        dcc.Input(
            id='search-bar-foreign',
            placeholder='Search University',
            type='text',
            style={'margin-bottom':20, 'margin-right':20},
            value='',
            className='two columns'
        ),
        html.Div(
            id='closest-matches-foreign',
            className='nine columns',
            style={'margin-bottom':20},
        ),
    ], className='row'),
    html.Div([
        dcc.Graph(id='choropleth-university-map-foreign', config={'displayModeBar': False},
                    style={'margin-bottom':8}, hoverData={'points': [{'customdata': 'University of Dhaka'}]},
                    className='six columns'),
        dcc.Graph(id='continent-bar-chart', config={'displayModeBar': False},
                    style={'margin-bottom':8},
                    className='six columns')
    ], className='row'),
    html.Div([
        dcc.Graph(id='choropleth-country-map-foreign', config={'displayModeBar': False},
                    style={'margin-bottom':8, 'height': 600},
                    className='nine columns'),
        html.Div([
            html.Div([
                dt.DataTable(id='info-table-countries', rows=[{}],
                                    filterable=True, sortable=True)
                ], id='info-table-countries-content',
                    style={'margin-top': 100, 'margin-bottom': 8},
                    className='three columns'),
            ], style={'height': 600, 'backgroundColor': '#CAD2D4'})
    ], className='row'),
    html.Div([
        html.Div(
            [
                html.P('Program Level:', style={'fontSize': 16}),
                dcc.Dropdown(
                    id='program-selector',
                    options=available_program_class,
                    multi=False,
                    clearable=False,
                    value='Bachelor'
                )
            ],
            className='two columns',
            style={'margin-top':10, 'margin-bottom':20}
        ),
    ], className='row'),
    html.Div([
        dcc.Graph(id='country-program-bar', config={'displayModeBar': False},
                    style={'margin-bottom':8},
                    className='six columns'),
        dcc.Graph(id='yearwise-line', config={'displayModeBar': False},
                    style={'margin-bottom':8},
                    className='six columns')
    ], className='row'),
    html.Div([
        html.Div([
            html.H5('Detail Information of All Foreign Students (2017)',
                        style={'margin-top': 20, 'text-align': 'center'}),
            dt.DataTable(id='details-count-foreign-table',
                            rows=details_count_foreign_dataframe.to_dict('records'),
                            columns=details_count_foreign_dataframe.columns,
                            filterable=True,
                            editable=False,
                            sortable=True)
            ], id='details-count-foreign-table-content',
                style={'margin-bottom': 50}),
    ], className='row')
], className='ten columns offset-by-one')

@app.callback(
    Output('choropleth-university-map-foreign', 'figure'),
    [Input('search-output-foreign', 'children')])
def update_choropleth_university_map_foreign(search_output):
    search_output_list = json.loads(search_output)
    filtered_locations_df = locations_foreign_df[locations_foreign_df['Name of University'].isin(search_output_list)]
    colors_dict = {'Dhaka': '#B6DEB9', 'Chittagong': '#C3D4B4', 'Barisal': '#9EC5B2', 'Sylhet': '#98C9C5',
                    'Rajshahi': '#7DBDB5', 'Khulna': '#C7D5BB', 'Mymensingh': '#97C6B0', 'Rangpur': '#BCDBBA'}
    choropleth_university_map_helpers_foreign.lats = filtered_locations_df['Latitude']
    choropleth_university_map_helpers_foreign.lons = filtered_locations_df['Longitude']

    choropleth_university_map_helpers_foreign.colors_dict = colors_dict
    university_text = filtered_locations_df['Name of University'].values.tolist()

    choropleth_university_map_helpers_foreign.text = university_text
    choropleth_university_map_helpers_foreign.name = university_text

    figure = choropleth_university_map_helpers_foreign.generate_map_figure('Location of Universities<br>with Foreign Students',
                                                                            23.7316957, 90.3965275, 5.2)
    return figure

@app.callback(
    Output('choropleth-country-map-foreign', 'figure'),
    [Input('selected-foreign-dataframe', 'children'),
    Input('selected-univ', 'children'),
    Input('continent-bar-chart', 'clickData')])
def update_choropleth_country_map_foreign(df, selected_univ, clickData):
    try:
        selected_continent = get_interaction_element(clickData, 'x')
        scope = continent_scope_mapper[selected_continent]
    except:
        scope = 'world'
    df = pd.DataFrame(json.loads(df))
    data = [dict(
        type = 'choropleth',
        locations = df['Code'],
        z = df['Total Students'],
        text = df['Country'],
        colorscale = 'Reds',
        autocolorscale = False,
        # reversescale = True,
        marker = dict(
            line = dict(
                color = 'rgb(180,180,180)',
                width = 0.5)
        ),
        colorbar = dict(
            autotick = False,
            title = 'No. of Foreign Students')
      )]

    layout = dict(
        title = 'No. of Foreign Students per Country in "{}"<br><i>Select continent from barchart above to change focus</i>'.format(selected_univ),
        geo = dict(
            scope=scope,
            showframe = False,
            projection = dict(
                type = 'equirectangular'
            )
        )
    )
    fig = dict(data=data, layout=layout)
    fig['layout'].update(chart_layout_template)
    return fig

@app.callback(
    Output('search-output-foreign', 'children'),
    [Input('search-bar-foreign', 'value')])
def search_foreign(search_string):
    if len(search_string) > 0:
        query = search_string
        choices = locations_foreign_df['Name of University'].values.tolist()
        closest_matches = process.extract(query, choices, scorer=fuzz.partial_ratio, limit=50)
        closest_matches_list = [i[0] for i in closest_matches if i[1] > 90]
        return json.dumps(closest_matches_list)
    return json.dumps(locations_foreign_df['Name of University'].values.tolist())

@app.callback(
    Output('closest-matches-foreign', 'children'),
    [Input('search-output-foreign', 'children')])
def closest_matches_foreign(search_output):
    search_output_str = search_output[1:-1]
    if len(json.loads(search_output)) < 48:
        return 'Closest matches (select from map below): ' + search_output_str
    return ''

@app.callback(
    Output('selected-foreign-dataframe', 'children'),
    [Input('selected-univ', 'children')])
def update_country_dataframe(selected_univ):
    filtered_df = countrywise_foreign_dataframe[countrywise_foreign_dataframe['Name of University']==selected_univ]
    df = filtered_df.groupby(['Country', 'Code', 'Continent']).sum()['Total Students'].reset_index()
    return json.dumps(df.to_dict())

@app.callback(
    Output('selected-univ', 'children'),
    [Input('choropleth-university-map-foreign', 'hoverData')])
def selected_univ(hoverData):
    selected_univ = get_interaction_element(hoverData)
    return selected_univ

@app.callback(
    Output('continent-bar-chart', 'figure'),
    [Input('selected-foreign-dataframe', 'children'),
    Input('selected-univ', 'children')])
def continent_barchart(df, selected_univ):
    df = pd.DataFrame(json.loads(df))
    df = df.groupby('Continent').sum()['Total Students'].reset_index()
    df = df.sort_values(by='Total Students', ascending=False)
    data = [go.Bar(
            x=df['Continent'],
            y=df['Total Students'],
            marker=dict(color='#902928')
    )]
    fig = go.Figure(data=data, layout=chart_layout_template)
    fig['layout'].update({'title': 'No. of Foreign Students per Continent<br>in "{}"'.format(selected_univ)})
    return fig

@app.callback(
    Output('info-table-countries-content', 'children'),
    [Input('selected-foreign-dataframe', 'children')])
def update_info_table(df):
    df = pd.DataFrame(json.loads(df))
    df = df.sort_values(by='Total Students', ascending=False)
    return html.Div([
        dt.DataTable(
            rows=df.to_dict('records'),
            columns=['Country', 'Continent', 'Total Students'],
            filterable=True,
            sortable=True,
            editable=False,
            id='info-table-countries'
        )
    ])

@app.callback(
    Output('country-program-bar', 'figure'),
    [Input('selected-univ', 'children'),
    Input('program-selector', 'value')])
def update_country_program_bar(selected_univ, selected_program):
    filtered_df = countrywise_foreign_dataframe[(countrywise_foreign_dataframe['Name of University']==selected_univ)
                                                                    & (countrywise_foreign_dataframe['Program Level']==selected_program)]
    if len(filtered_df) == 0:
        fig = go.Figure(layout = chart_layout_template)
        fig['layout']['title'] = 'No. of Foreign Students ({0}) by Gender and Country<br>in "{1}"'\
                                    .format(selected_program, selected_univ)
        return fig
    filtered_df = filtered_df.sort_values(by='Total Students', ascending=False)
    x_axis_categories = filtered_df['Country'].values.tolist()
    y_data_list = [filtered_df['Male'], filtered_df['Female']]
    # colors_list = ['#7DBDB5', '#B6DEB9']
    colors_list = ['#902928', '#BB5A58']
    names_list = ['Male', 'Female']
    chart_title = 'No. of Foreign Students ({0}) by Gender and Country<br>in "{1}"'\
                    .format(selected_program, selected_univ)
    return generate_barchart(x_axis_categories, y_data_list,
                                        colors_list, names_list, chart_title)

@app.callback(
    Output('yearwise-line', 'figure'),
    [Input('selected-univ', 'children')])
def update_yearwise_line(selected_univ):
    filtered_df = yearwise_foreign_dataframe[yearwise_foreign_dataframe['Name of University']==selected_univ]
    if len(filtered_df) == 0:
        fig = go.Figure(layout = chart_layout_template)
        fig['layout']['title'] = 'No. of Foreign Students by Year<br>in "{}"'\
                                    .format(selected_univ)
        return fig
    filtered_chart_df = pd.melt(filtered_df, value_vars=np.setdiff1d(filtered_df.columns, ['Country', 'Name of University', 'Sl.']))
    filtered_chart_df = filtered_chart_df.rename(columns={'variable': 'Year', 'value': 'Total Students'})
    filtered_chart_df = filtered_chart_df.sort_values(by='Year')

    return generate_linechart(None, filtered_chart_df,
                        'Total Students',
                        'No. of Foreign Students (Total) by Year<br>in "{}"'.format(selected_univ),
                        processed=True)
