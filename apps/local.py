import dash_core_components as dcc
import dash_html_components as html
import dash_table_experiments as dt
from dash.dependencies import Input, Output
import plotly.graph_objs as go
from helpers import *
import json
from app import app
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

dashboard_name = 'University Students Dashboard'

location_file_helpers = FileHelpers('./data/cleaned/locations.xlsx', 'locations')
locations_dataframe = location_file_helpers.get_raw_dataframe()

enrolled_file_helpers = FileHelpers('./data/cleaned/Statistics of enrolled students_2017.xlsx', 'Sheet1')
enrolled_dataframe = enrolled_file_helpers.get_raw_dataframe()

residential_file_helpers = FileHelpers('./data/cleaned/Statistics of residential students_2017.xlsx', 'Sheet1')
residential_dataframe = residential_file_helpers.get_raw_dataframe()

expenditure_file_helpers = FileHelpers('./data/cleaned/Annual expenditure per student on financial year basis.xlsx', 'Sheet1')
expenditure_dataframe = expenditure_file_helpers.get_raw_dataframe()

yearwise_students_file_helpers = FileHelpers('./data/cleaned/Yearwise total number of students in different universities including affiliated colleges and madrashas.xlsx', 'Sheet1')
yearwise_students_dataframe = yearwise_students_file_helpers.get_raw_dataframe()

scholarships_file_helpers = FileHelpers('./data/cleaned/Statistics of full free, scholarship and waiver received students_2017.xlsx', 'Sheet1')
scholarships_dataframe = scholarships_file_helpers.get_raw_dataframe()

program_wise_file_helpers = FileHelpers('./data/cleaned/Program levelwise student statistics in Public Universities_National University_Open University and Affiliated Colleges_Madrasas of Public University_2017.xlsx', 'editted 1')
program_wise_dataframe = program_wise_file_helpers.get_raw_dataframe()
program_wise_dataframe_helpers = DataFrameHelpers(program_wise_dataframe, None)
available_program_list = program_wise_dataframe_helpers.get_available_levels('Program')
available_program_class = program_wise_dataframe_helpers.get_selector_options_class(available_program_list,
                                                                                ['Bachelors', 'Masters', 'Mphil', 'PhD', 'Others'], is_ascending=True)

major_wise_file_helpers = FileHelpers('./data/cleaned/Major subjectwise student statistics_2017.xlsx', 'Eddited')
major_wise_dataframe = major_wise_file_helpers.get_raw_dataframe()
major_wise_dataframe_helpers = DataFrameHelpers(major_wise_dataframe, None)
available_major_list = major_wise_dataframe_helpers.get_available_levels('Major')
available_major_class = major_wise_dataframe_helpers.get_selector_options_class(available_major_list,
                                                                                ['Science, Technology, Medical, Agriculture and Engineering',
                                                                                'Arts, Humanities and Social Science',
                                                                                'Commerce, Business and Trade',
                                                                                'Law',
                                                                                'Education'], is_ascending=True)

all_details_file_helpers = FileHelpers('./data/cleaned/merged_details.xlsx', 'Sheet1')
all_details_dataframe = all_details_file_helpers.get_raw_dataframe()
all_details_dataframe_helpers = DataFrameHelpers(all_details_dataframe, None)
all_details_column_list = all_details_dataframe.columns.tolist()
available_table_columns_class = all_details_dataframe_helpers.get_selector_options_class(all_details_column_list[1:],
                                                                                all_details_column_list[1:], is_ascending=True)

layout_helpers = FixedLayoutHelpers(dashboard_name)
firstrow_layout = layout_helpers.generate_firstrow_layout(button_class_list=['button button-selected', 'button'])
secondrow_layout = layout_helpers.generate_secondrow_layout()

map_data_url = 'https://gist.githubusercontent.com/kmjawadurrahman/edb369357eccae6b2d52a081bb1e66c9/raw/b6b2d435b30e226b9a28496b844db0dcc616b1ff/bd-division.json'

choropleth_university_map_helpers = ChoroplethMapHelpers(map_data_url)

choropleth_university_map_helpers.load_map_data()

layout = html.Div([
    html.Div(
        firstrow_layout, className='row', style={'margin-top':20, 'margin-bottom':20}
    ),
    html.Div(
        secondrow_layout, className='row', style={'margin-bottom':20}
    ),
    html.H4('University Level Statistics'),
    html.Div(
        id='search-output', style={'display': 'none'}
    ),
    html.Div([
        dcc.Input(
            id='search-bar',
            placeholder='Search University',
            type='text',
            style={'margin-bottom':20, 'margin-right':20},
            value='',
            className='two columns'
        ),
        html.Div(
            id='closest-matches',
            className='nine columns',
            style={'margin-bottom':20},
        ),
    ], className='row'),
    html.Div([
        dcc.Graph(id='choropleth-university-map', config={'displayModeBar': False},
                    style={'margin-bottom':8}, hoverData={'points': [{'customdata': 'University of Dhaka'}]},
                    className='six columns'),
        html.Div(id='info-div',
                    style={'margin-bottom':8},
                    className='six columns')
    ], className='row'),
    html.Div([
        dcc.Graph(id='own-students-line', config={'displayModeBar': False}, style={'margin-bottom':8}, className='six columns'),
        dcc.Graph(id='annual-expenditure-line', config={'displayModeBar': False}, style={'margin-bottom':8}, className='six columns'),
    ], className='row'),
    html.Div([
        dcc.Graph(id='program-wise-bar', config={'displayModeBar': False}, style={'margin-bottom':8}, className='six columns'),
        dcc.Graph(id='residential-wise-bar', config={'displayModeBar': False}, style={'margin-bottom':8}, className='six columns'),
    ], className='row'),
    html.Div([
        dcc.Graph(id='scholarship-type-bar', config={'displayModeBar': False}, style={'margin-bottom':8}, className='five columns'),
        dcc.Graph(id='scholarship-gender-bar', config={'displayModeBar': False}, style={'margin-bottom':8}, className='five columns'),
        html.Div([
            html.Div(id='waiver-amount-male', className='row'),
            html.Div(id='waiver-amount-female', className='row')
        ], className='two columns', style={'height': '450px'})
    ], className='row'),
    html.H4('All University Combined Statistics', style={'margin-top': '50px'}),
    html.Div([
        html.Div([
            html.Div(
                [
                    html.P('Program Level:', style={'fontSize': 16}),
                    dcc.Dropdown(
                        id='program-selector-local',
                        options=available_program_class,
                        multi=False,
                        clearable=False,
                        value='Bachelors'
                    )
                ],
                className='six columns',
                style={'margin-bottom':20}
            ),
            dcc.Graph(id='univ-category-program-bar', config={'displayModeBar': False}, style={'margin-bottom':8}, className='twelve columns'),
        ], className='five columns'),
        html.Div([
            html.Div(
                [
                    html.P('Major Subject:', style={'fontSize': 16}),
                    dcc.Dropdown(
                        id='major-selector-local',
                        options=available_major_class,
                        multi=False,
                        clearable=False,
                        value='Science, Technology, Medical, Agriculture and Engineering'
                    )
                ],
                className='ten columns',
                style={'margin-bottom':20}
            ),
            dcc.Graph(id='univ-category-major-bar', config={'displayModeBar': False}, style={'margin-bottom':8}, className='twelve columns'),
        ], className='five columns'),
        html.Div([
            html.Div(id='univ-category-program-total', className='row'),
            html.Div(id='univ-category-major-total', className='row')
        ], className='two columns', style={'margin-top':'88px', 'height': '450px'})
    ], className='row'),
    html.H5('Detail Information of All Students ({})'.format(max(enrolled_dataframe['Year'])),
                style={'margin-top': 20, 'text-align': 'center'}),
    html.Div(
        [
            html.P('Add or Remove Table Columns:', style={'fontSize': 16}),
            dcc.Dropdown(
                id='table-columns-selector',
                options=available_table_columns_class,
                multi=True,
                clearable=True,
                value=['Total No. of Students',
                        'Total Expenditure Per Student', 'Total No. of Residential Students',
                        'No. of Free Studentship - Total', 'No. of Scholarships Awarded - Total',
                        'No. of Waiver - Total', 'Amount of Waiver - Total']
            )
        ],
        className='row',
        style={'margin-top':20, 'margin-bottom':20}
    ),
    html.Div([
        html.Div([
            dt.DataTable(rows=[{}])
        ], id='student-details-info-table-content',
                style={'margin-bottom': 50}),
    ], className='row')
], className='ten columns offset-by-one')

@app.callback(
    Output('choropleth-university-map', 'figure'),
    [Input('search-output', 'children')])
def update_choropleth_university_map(search_output):
    search_output_list = json.loads(search_output)
    filtered_locations_df = locations_dataframe[locations_dataframe['Name of University'].isin(search_output_list)]
    colors_dict = {'Dhaka': '#B6DEB9', 'Chittagong': '#C3D4B4', 'Barisal': '#9EC5B2', 'Sylhet': '#98C9C5',
                    'Rajshahi': '#7DBDB5', 'Khulna': '#C7D5BB', 'Mymensingh': '#97C6B0', 'Rangpur': '#BCDBBA'}
    choropleth_university_map_helpers.lats = filtered_locations_df['Latitude']
    choropleth_university_map_helpers.lons = filtered_locations_df['Longitude']

    choropleth_university_map_helpers.colors_dict = colors_dict
    university_text = filtered_locations_df['Name of University'].values.tolist()

    choropleth_university_map_helpers.text = university_text
    choropleth_university_map_helpers.name = university_text

    figure = choropleth_university_map_helpers.generate_map_figure('University Locations', 23.7316957, 90.3965275, 5.2)
    return figure

@app.callback(
    Output('search-output', 'children'),
    [Input('search-bar', 'value')])
def search(search_string):
    if len(search_string) > 0:
        query = search_string
        choices = locations_dataframe['Name of University'].values.tolist()
        closest_matches = process.extract(query, choices, scorer=fuzz.partial_ratio, limit=50)
        closest_matches_list = [i[0] for i in closest_matches if i[1] > 90]
        return json.dumps(closest_matches_list)
    return json.dumps(locations_dataframe['Name of University'].values.tolist())

@app.callback(
    Output('info-div', 'children'),
    [Input('choropleth-university-map', 'hoverData')])
def info_div(hoverData):
    selected_univ = get_interaction_element(hoverData)
    filtered_enrolled_df = enrolled_dataframe[enrolled_dataframe['Name of University']==selected_univ]
    filtered_residential_df = residential_dataframe[residential_dataframe['Name of University']==selected_univ]
    filtered_expenditure_df = expenditure_dataframe[expenditure_dataframe['Name of University']==selected_univ]

    latest_year = max(filtered_enrolled_df['Year'])
    filtered_enrolled_df_latest_year = filtered_enrolled_df[filtered_enrolled_df['Year']==latest_year]
    filtered_residential_df_latest_year = filtered_residential_df[filtered_residential_df['Year']==latest_year]
    filtered_expenditure_df_latest_year = filtered_expenditure_df[filtered_expenditure_df['Year']==latest_year]
    try:
        total_students = str(filtered_enrolled_df_latest_year['Total'].sum())
    except:
        total_students = 'n/a'
    try:
        total_halls = str(filtered_residential_df_latest_year['Total Number of Halls/ Dormitories'].values[0])
    except:
        total_halls = 'n/a'
    try:
        total_residential = str(filtered_residential_df_latest_year['% of Residential Students Against Total Students (Total)'].values[0]) + '%'
    except:
        total_residential = 'n/a'
    try:
        total_expenditure = str(filtered_expenditure_df_latest_year['Total Expenditure Per Student'].values[0])
    except:
        total_expenditure = 'n/a'
    return html.Div([
                html.H4(selected_univ + ' ({} statistics)'.format(latest_year), style={'margin-bottom': 50, 'margin-left': 20}),
                html.H5('Total Students: ' + total_students,
                            style={'margin-bottom': 30, 'margin-left': 50}),
                html.H5('Total Halls/Dormitories: ' + total_halls,
                            style={'margin-bottom': 30, 'margin-left': 50}),
                html.H5('% of Residential Students Against Total Students: ' + total_residential,
                            style={'margin-bottom': 30, 'margin-left': 50}),
                html.H5('Total Expenditure per Student: ' + total_expenditure,
                            style={'margin-bottom': 30, 'margin-left': 50}),
            ], style={'margin-left': 20})

@app.callback(
    Output('closest-matches', 'children'),
    [Input('search-output', 'children')])
def closest_matches(search_output):
    search_output_str = search_output[1:-1]
    if len(json.loads(search_output)) < 52:
        return 'Closest matches (select from map below): ' + search_output_str
    return ''

@app.callback(
    Output('own-students-line', 'figure'),
    [Input('choropleth-university-map', 'hoverData')])
def update_own_students_line(hoverData):
    selected_univ = get_interaction_element(hoverData)
    return generate_linechart(selected_univ, expenditure_dataframe, 'Total Number of Students',
                                'Total Students by Year<br>in "{}"'.format(selected_univ))

@app.callback(
    Output('annual-expenditure-line', 'figure'),
    [Input('choropleth-university-map', 'hoverData')])
def update_expenditure_line(hoverData):
    selected_univ = get_interaction_element(hoverData)
    return generate_linechart(selected_univ, expenditure_dataframe, 'Total Expenditure Per Student',
                                'Total Expenditure per Student by Year<br>in "{}"'.format(selected_univ))

@app.callback(
    Output('program-wise-bar', 'figure'),
    [Input('choropleth-university-map', 'hoverData')])
def update_program_bar(hoverData):
    selected_univ = get_interaction_element(hoverData)
    filtered_df = enrolled_dataframe[(enrolled_dataframe['Name of University']==selected_univ)]
    if len(filtered_df) == 0:
        fig = go.Figure(layout = chart_layout_template)
        fig['layout']['title'] = 'Total Students by Program Level and Gender<br>in "{}"'.format(selected_univ)
        return fig
    x_axis_categories = filtered_df['Program Level'].values.tolist()
    y_data_list = [filtered_df['Male'], filtered_df['Female']]
    colors_list = ['#902928', '#BB5A58']
    names_list = ['Male', 'Female']
    chart_title = 'Total Students by Program Level and Gender ({0})<br>in "{1}"'.format(max(filtered_df['Year']), selected_univ)
    return generate_barchart(x_axis_categories, y_data_list, colors_list, names_list, chart_title)

@app.callback(
    Output('residential-wise-bar', 'figure'),
    [Input('choropleth-university-map', 'hoverData')])
def update_residential_bar(hoverData):
    selected_univ = get_interaction_element(hoverData)
    filtered_df = residential_dataframe[(residential_dataframe['Name of University']==selected_univ)]
    if len(filtered_df) == 0:
        fig = go.Figure(layout = chart_layout_template)
        fig['layout']['title'] = 'Total Students by Residency and Gender<br>in "{}"'.format(selected_univ)
        return fig
    x_axis_categories = ['Residential', 'Non-residential']
    y_data_list = [[filtered_df['No. of Male Residential Students'].values.tolist()[0],
                        filtered_df['No. of Male Non Residential Students'].values.tolist()[0]],
                    [filtered_df['No. of Female Residential Students'].values.tolist()[0],
                        filtered_df['No. of Female Non Residential Students'].values.tolist()[0]]]
    colors_list = ['#902928', '#BB5A58']
    names_list = ['Male', 'Female']
    chart_title = 'Total Students by Residency and Gender ({0})<br>in "{1}"'.format(max(filtered_df['Year']), selected_univ)
    return generate_barchart(x_axis_categories, y_data_list, colors_list, names_list, chart_title)

@app.callback(
    Output('scholarship-type-bar', 'figure'),
    [Input('choropleth-university-map', 'hoverData')])
def update_scholarship_type_bar(hoverData):
    selected_univ = get_interaction_element(hoverData)
    filtered_df = scholarships_dataframe[(scholarships_dataframe['Name of University']==selected_univ)]
    if len(filtered_df) == 0:
        fig = go.Figure(layout = chart_layout_template)
        fig['layout']['title'] = 'Free Studentship <b>(% of Total)</b> by Type<br>in "{}"'.format(selected_univ)
        fig['layout']['xaxis'].update(dict(tickangle=10))
        fig['layout']['font'].update(dict(size=10))
        return fig
    x_axis_categories = ['Merit Scholarship', 'Need Based', 'Freedom Fighters\' Children', 'Others']
    y_data_list = [[filtered_df['No. of Free Studentship - Merit Scholarship (per Student)'].values.tolist()[0],
                        filtered_df['No. of Free Studentship - Need Based (per Student)'].values.tolist()[0],
                    filtered_df['No. of Free Studentship - Freedom Fighters\' Children (per Student)'].values.tolist()[0],
                        filtered_df['No. of Free Studentship - Others (per Student)'].values.tolist()[0]]]
    colors_list = ['#902928']
    names_list = ['Total']
    chart_title = 'Free Studentship <b>(% of Total)</b> by Type ({0})<br>in "{1}"'.format(max(filtered_df['Year']), selected_univ)
    figure = generate_barchart(x_axis_categories, y_data_list, colors_list, names_list, chart_title)
    figure['layout']['xaxis'].update(dict(tickangle=8))
    figure['layout']['font'].update(dict(size=10))
    return figure

@app.callback(
    Output('scholarship-gender-bar', 'figure'),
    [Input('choropleth-university-map', 'hoverData')])
def update_scholarship_gender_bar(hoverData):
    selected_univ = get_interaction_element(hoverData)
    filtered_df = scholarships_dataframe[(scholarships_dataframe['Name of University']==selected_univ)]
    if len(filtered_df) == 0:
        fig = go.Figure(layout = chart_layout_template)
        fig['layout']['title'] = 'Scholarship and Waiver <b>(% of Gender Total)</b> by Gender<br>in "{}"'.format(selected_univ)
        fig['layout']['font'].update(dict(size=10))
        return fig
    x_axis_categories = ['Scholarship Awarded', 'Waiver']
    y_data_list = [[filtered_df['No. of Scholarships Awarded - Male (per Male Student)'].values.tolist()[0],
                        filtered_df['No. of Waiver - Male (per Male Student)'].values.tolist()[0]],
                    [filtered_df['No. of Scholarships Awarded - Female (per Female Student)'].values.tolist()[0],
                        filtered_df['No. of Waiver - Female (per Female Student)'].values.tolist()[0]]]
    colors_list = ['#902928', '#BB5A58']
    names_list = ['Male', 'Female']
    chart_title = 'Scholarship and Waiver <b>(% of Gender Total)</b> by Gender ({0})<br>in "{1}"'.format(max(filtered_df['Year']), selected_univ)
    figure = generate_barchart(x_axis_categories, y_data_list, colors_list, names_list, chart_title, barmode='group')
    figure['layout']['font'].update(dict(size=10))
    return figure

@app.callback(
    Output('waiver-amount-male', 'children'),
    [Input('choropleth-university-map', 'hoverData')])
def update_waiver_amount_male(hoverData):
    selected_univ = get_interaction_element(hoverData)
    filtered_df = scholarships_dataframe[(scholarships_dataframe['Name of University']==selected_univ)]
    if len(filtered_df) == 0:
        return 'n/a'
    value = str(round(filtered_df['Amount of Waiver - Male (per Male Student)'].values[0], 1))
    title = 'Amount of Waiver (per Male)'
    return generate_scorecard(value, title)

@app.callback(
    Output('waiver-amount-female', 'children'),
    [Input('choropleth-university-map', 'hoverData')])
def update_waiver_amount_female(hoverData):
    selected_univ = get_interaction_element(hoverData)
    filtered_df = scholarships_dataframe[(scholarships_dataframe['Name of University']==selected_univ)]
    if len(filtered_df) == 0:
        return 'n/a'
    value = str(round(filtered_df['Amount of Waiver - Female (per Female Student)'].values[0], 1))
    title = 'Amount of Waiver (per Female)'
    return generate_scorecard(value, title)

@app.callback(
    Output('univ-category-program-bar', 'figure'),
    [Input('program-selector-local', 'value')])
def update_program_gender_bar(program_name):
    filtered_df = program_wise_dataframe[(program_wise_dataframe['Program']==program_name)]
    if len(filtered_df) == 0:
        fig = go.Figure(layout = chart_layout_template)
        fig['layout']['title'] = 'Program Level-wise Student Statistics by Gender and<br>University Type: "{}"'.format(program_name)
        fig['layout']['xaxis'].update(dict(tickangle=8))
        fig['layout']['font'].update(dict(size=10))
        return fig
    x_axis_categories = filtered_df['University'].values.tolist()
    y_data_list = [filtered_df['Male'], filtered_df['Female']]
    colors_list = ['#902928', '#BB5A58']
    names_list = ['Male', 'Female']
    chart_title = 'Program Level-wise Student Statistics ({0}) by Gender and<br>University Type: "{1}"'\
                    .format(max(enrolled_dataframe['Year']), program_name)
    figure = generate_barchart(x_axis_categories, y_data_list, colors_list, names_list, chart_title)
    figure['layout']['xaxis'].update(dict(tickangle=8))
    figure['layout']['font'].update(dict(size=10))
    return figure

@app.callback(
    Output('univ-category-major-bar', 'figure'),
    [Input('major-selector-local', 'value')])
def update_major_gender_bar(major_name):
    filtered_df = major_wise_dataframe[(major_wise_dataframe['Major']==major_name)]
    if len(filtered_df) == 0:
        fig = go.Figure(layout = chart_layout_template)
        fig['layout']['title'] = 'Major Subject-wise Student Statistics by Gender and<br>University Type: "{}"'.format(major_name)
        fig['layout']['xaxis'].update(dict(tickangle=8))
        fig['layout']['font'].update(dict(size=10))
        return fig
    x_axis_categories = filtered_df['University'].values.tolist()
    y_data_list = [filtered_df['Male'], filtered_df['Female']]
    colors_list = ['#902928', '#BB5A58']
    names_list = ['Male', 'Female']
    chart_title = 'Major Subject-wise Student Statistics ({0}) by Gender and<br>University Type: "{1}"'\
                    .format(max(enrolled_dataframe['Year']), major_name)
    figure = generate_barchart(x_axis_categories, y_data_list, colors_list, names_list, chart_title)
    figure['layout']['xaxis'].update(dict(tickangle=8))
    figure['layout']['font'].update(dict(size=10))
    return figure

@app.callback(
    Output('univ-category-program-total', 'children'),
    [Input('program-selector-local', 'value')])
def update_program_total(program_name):
    filtered_df = program_wise_dataframe[(program_wise_dataframe['Program']==program_name)]
    if len(filtered_df) == 0:
        return 'n/a'
    value = str(int(filtered_df['Total'].sum()))
    title = 'Total Students for Program'
    return generate_scorecard(value, title)

@app.callback(
    Output('univ-category-major-total', 'children'),
    [Input('major-selector-local', 'value')])
def update_major_total(major_name):
    filtered_df = major_wise_dataframe[(major_wise_dataframe['Major']==major_name)]
    if len(filtered_df) == 0:
        return 'n/a'
    value = str(int(filtered_df['Total'].sum()))
    title = 'Total Students for Major'
    return generate_scorecard(value, title)

@app.callback(
    Output('student-details-info-table-content', 'children'),
    [Input('table-columns-selector', 'value')])
def update_all_details_table(columns_list):
    return html.Div([
                dt.DataTable(
                    rows=all_details_dataframe.to_dict('records'),
                    columns=['Name of University'] + columns_list,
                    filterable=True,
                    editable=False,
                    sortable=True)
            ])
